# Narnia

I got this CTF recommended to me as a good starting point `http://overthewire.org/wargames/narnia/`.

The CTF consists of several challanges where each grants you the user rights to access a password to the next level in `/etc/narnia_pass/`.

The first level is accessed by ssh-ing to their server with the username and pass "narnia0"
```ssh -l narnia0 narnia.labs.overthewire.org```.

All files are placed in /games/narnia/ and follow the same fileowner structure: 

```
-r-sr-x--- 1 narnia1 narnia0 7452 Nov 14  2014 narnia0
-r--r----- 1 narnia0 narnia0 1138 Nov 14  2014 narnia0.c
```

### Index 

[Narnia 0-4](narnia0-4/readme.md)    
[Narnia 5-9](narnia5-9/readme.md)