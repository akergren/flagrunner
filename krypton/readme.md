# Krypton

### Krypton0
The first level is just to de-encode the base64 string given from the webpage and then use the key to login to krypto1.

### Krypton1
The readme file tells you that the key is a ROT13 "encrypted" file, the wiki page for ROT13 even have as nice oneliner to use for translating this encryption:
	`tr 'A-Za-z' 'N-ZA-Mn-za-m'` 

### Krypton2
This is a similar encoding to the ROT13, although you do not have the translation key this time.

I solved it by encrypting the alphabet and analyse how the chars change. 

I found that A->M, N->Z, O->A and Z->L. Using the `tr` tool to translate I found this regex to work well:
	`tr 'M-Zm-zA-La-l' 'A-Na-nO-Z'`

### Krypton3
tr 'SQVJD' 'EASTH'